#pragma once
#include <list>
#include <map>
#include <vector>
#include <sqlite3.h>
#include <iostream>
#include "Album.h"
#include "User.h"

class DatabaseHelper
{
public: 

	/// <summary>
	/// this callback reads the data_record and formats it inside the data var (best used with picture and album objects)
	/// </summary>
	/// <param name="data">a vector of of all the rows, each row is formated by: col_name - col_val (key - value)</param>
	/// <param name="argc">The number of columns in row</param>
	/// <param name="argv">An array of strings representing fields in the row</param>
	/// <param name="azColName">An array of strings representing column names</param>
	/// <returns></returns>
	static int generalObjectCallback(void* data, int argc, char** argv, char** azColName);

	/// <summary>
	/// this callback is used when selecting users from the database
	/// </summary>
	/// <param name="data">will be a list of users</param>
	/// <param name="argc">The number of columns in row</param>
	/// <param name="argv">An array of strings representing fields in the row</param>
	/// <param name="azColName">An array of strings representing column names</param>
	/// <returns></returns>
	static int userTableCallback(void* data, int argc, char** argv, char** azColName);
};

