#include "DatabaseAccess.h"
#include "ItemNotFoundException.h"

DatabaseAccess::~DatabaseAccess()
{
	this->close();
}

/*======================================================database related======================================================*/
bool DatabaseAccess::open()
{
	std::string dbFileName = "C:\\sqlite\\galleryDB.sqlite";

	int res = sqlite3_open(dbFileName.c_str(), &this->_DB);
	if (res != SQLITE_OK) {
		this->_DB = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return false;
	}

	/*-------------------------make sure all the tables exist-------------------------*/
	std::string createStatement1 = "CREATE TABLE IF NOT EXISTS USERS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL);";
	std::string createStatement2 = "CREATE TABLE IF NOT EXISTS ALBUMS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL, USER_ID INTEGER NOT NULL, CREATION_DATE TEXT NOT NULL, FOREIGN KEY(USER_ID) REFERENCES USERS (ID));";
	std::string createStatement3 = "CREATE TABLE IF NOT EXISTS PICTURES ( ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL, LOCATION TEXT NOT NULL,CREATION_DATE TEXT NOT NULL, ALBUM_ID INTEGER NOT NULL, FOREIGN KEY(ALBUM_ID ) REFERENCES ALBUMS (ID)); ";
	std::string createStatement4 = "CREATE TABLE IF NOT EXISTS TAGS(PICTURE_ID INTEGER NOT NULL, USER_ID INTEGER NOT NULL, PRIMARY KEY(PICTURE_ID,USER_ID), FOREIGN KEY(PICTURE_ID ) REFERENCES PICTURES (ID), FOREIGN KEY(USER_ID ) REFERENCES USERS (ID));";


	if (sqlite3_exec(this->_DB, createStatement1.c_str(), nullptr, nullptr, 0) != SQLITE_OK) {
		std::cerr << "Error: didn't create table1";
	}
	if (sqlite3_exec(this->_DB, createStatement2.c_str(), nullptr, nullptr, 0) != SQLITE_OK) {
		std::cerr << "Error: didn't create table2";
	}
	if (sqlite3_exec(this->_DB, createStatement3.c_str(), nullptr, nullptr, 0) != SQLITE_OK) {
		std::cerr << "Error: didn't create table3";
	}
	if (sqlite3_exec(this->_DB, createStatement4.c_str(), nullptr, nullptr, 0) != SQLITE_OK) {
		std::cerr << "Error: didn't create table4";
	}
	return true;
}

void DatabaseAccess::close()
{
	sqlite3_close(this->_DB);
	this->_DB = nullptr;
}

Album DatabaseAccess::data_record_to_album(std::map<std::string, std::string> data_record)
{
	Album album;
	album.setCreationDate(data_record.find("CREATION_DATE")->second);
	album.setName(data_record.find("NAME")->second);
	album.setOwner(atoi(data_record.find("USER_ID")->second.c_str()));

	//--------------------------------get all pictures of the album----------------------------------------//
	std::vector < std::map<std::string, std::string> > pictureRecords;
	std::string sqlQuery("SELECT * FROM pictures WHERE album_id = " + data_record.find("ID")->second + "; ");

	// get a vector of all the pictures of the album
	if (sqlite3_exec(this->_DB, sqlQuery.c_str(), DatabaseHelper::generalObjectCallback, &pictureRecords, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		std::cerr << "Error: couldn't SELECT pictures" << std::endl;
	}
	else
	{
		// make all the picture records into a picture object and add them to the album
		for (std::map<std::string, std::string> record : pictureRecords)
		{
			album.addPicture(this->data_record_to_picture(record));
		}
	}

	return album;
}

Picture DatabaseAccess::data_record_to_picture(std::map<std::string, std::string> data_record)
{
	int id = atoi(data_record.find("ID")->second.c_str()); // find id
	std::string name = data_record.find("NAME")->second; // find name
	std::string path = data_record.find("LOCATION")->second; // find path
	std::string creation_date = data_record.find("CREATION_DATE")->second; // find creation date

	Picture pic(id, name, path, creation_date); // make the picture object

	std::string findIdQuery = "SELECT user_id FROM tags WHERE picture_id = " + data_record.find("ID")->second + "; ";

	// get all tags of the picture
	sqlite3_stmt* stmt;
	if (sqlite3_prepare_v2(this->_DB, findIdQuery.c_str(), -1, &stmt, NULL))
	{
		printf("Error getting tags");
	}
	else // add tags
	{
		while (sqlite3_step(stmt) != SQLITE_DONE) // add the tags of the picture
		{
			pic.tagUser(sqlite3_column_int(stmt, 0));
		}
		sqlite3_finalize(stmt);
	}

	return pic;
}

void DatabaseAccess::finalizeAlbumRemoval()
{
	// delete all pictures that have an album_id that doesn't exist anymore
	std::string deleteQuery = "DELETE FROM pictures WHERE album_id NOT IN (SELECT id FROM albums);";

	// delete all pictures 
	if (sqlite3_exec(this->_DB, deleteQuery.c_str(), nullptr, nullptr, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		std::cerr << "Error: couldn't DELETE pictures" << std::endl;
	}
	else // if all pictures removed successfully, remove all tags with the picture id 
	{
		this->finalizePictureRemoval();
	}
}

void DatabaseAccess::finalizePictureRemoval()
{
	// delete all tags that have a picture)id that doesn't exist anymore
	std::string deleteQuery = "DELETE FROM tags WHERE picture_id NOT IN (SELECT id FROM pictures);";

	// delete all tags 
	if (sqlite3_exec(this->_DB, deleteQuery.c_str(), nullptr, nullptr, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		std::cerr << "Error: couldn't DELETE tags" << std::endl;
	}
}

void DatabaseAccess::finalizeUserRemoval()
{
	std::string deleteRelatedAlbumsQuery = "DELETE FROM albums WHERE user_id NOT IN (SELECT id FROM users);";
	std::string deleteRelatedTagsQuery = "DELETE FROM tags WHERE user_id NOT IN (SELECT id FROM users);";

	// delete all related albums (delete all albums that have a user_id that doesn't exist anymore)
	if (sqlite3_exec(this->_DB, deleteRelatedAlbumsQuery.c_str(), nullptr, nullptr, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		std::cerr << "Error: couldn't DELETE albums" << std::endl;
	}
	else // if all albums removed successfully, remove all pictures that have the album id erased
	{
		this->finalizeAlbumRemoval();
	}

	// delete all related tags (delete all tags that have a user_id that doesn't exist anymore)
	if (sqlite3_exec(this->_DB, deleteRelatedTagsQuery.c_str(), nullptr, nullptr, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		std::cerr << "Error: couldn't DELETE tags" << std::endl;
	}
}

/*======================================================albums related======================================================*/
void DatabaseAccess::printAlbums()
{
	std::list<Album> albums = this->getAlbums();
	if (albums.empty()) {
		throw MyException("There are no existing albums.");
	}
	std::cout << "Album list:" << std::endl;
	std::cout << "-----------" << std::endl;
	for (const Album& album : albums) {
		std::cout << std::setw(5) << "* " << album;
	}
}

const std::list<Album> DatabaseAccess::getAlbums()
{
	std::list<Album> albums;
	std::vector < std::map<std::string, std::string> > albumRecords;
	std::string sqlQuery("SELECT * FROM albums;");

	// send a query to the database (save the answer inside albumRecords)
	if (sqlite3_exec(this->_DB, sqlQuery.c_str(), DatabaseHelper::generalObjectCallback, &albumRecords, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		std::cerr << "Error: couldn't SELECT albums" << std::endl;
	}
	else // convert all records into a list of albums
	{
		// make all the album records to an album list
		for (std::map<std::string, std::string> record : albumRecords)
		{
			Album album = this->data_record_to_album(record); // go through each record and format it as an album object
			albums.push_back(album);
		}
	}

	return albums;
}

const std::list<Album> DatabaseAccess::getAlbumsOfUser(const User& user)
{
	std::list<Album> albumsOfUser;
	std::vector < std::map<std::string, std::string> > albumRecords;
	std::string sqlQuery = "SELECT * FROM albums WHERE user_id = " + std::to_string(user.getId()) + "; ";

	// send a query to the database (save the answer inside albumRecords
	if (sqlite3_exec(this->_DB, sqlQuery.c_str(), DatabaseHelper::generalObjectCallback, &albumRecords, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		std::cerr << "Error: couldn't SELECT albums of the user" << std::endl;
	}
	else
	{
		// make all the album records to an album list
		for (std::map<std::string, std::string> record : albumRecords)
		{
			Album album = this->data_record_to_album(record); // go through each record and format it as an album object
			albumsOfUser.push_back(album);
		}
	}

	return albumsOfUser;
}

void DatabaseAccess::createAlbum(const Album& album)
{
	std::string sqlQuery = "INSERT INTO albums (name, creation_date, user_id) VALUES(?, ?, ?);"; // query to insert the album and get it's id
	sqlite3_stmt* stmt;

	// prepare statement
	if (sqlite3_prepare_v2(this->_DB, sqlQuery.c_str(), -1, &stmt, NULL)) // check that nothing went wrong
	{
		std::cerr << "Error: couldn't prepare the INSERT album statement" << std::endl;
		return;
	}
	else // insert the album and all it's pictures
	{
		// bind values to the statement
		std::string date = album.getCreationDate();
		sqlite3_bind_text(stmt, 1, album.getName().c_str(), -1, NULL); // bind name
		sqlite3_bind_text(stmt, 2, date.c_str(), -1, NULL); // bind creation date
		sqlite3_bind_int(stmt, 3, album.getOwnerId()); // bind user id

		// run statement and close allocated memory
		sqlite3_step(stmt);
		sqlite3_finalize(stmt);

		// get the album id
		if (sqlite3_prepare_v2(this->_DB, "SELECT last_insert_rowid();", -1, &stmt, NULL))
		{
			printf("Error retrieving album id");
			return;
		}

		sqlite3_step(stmt);
		int albumId = sqlite3_column_int(stmt, 0);
		sqlite3_finalize(stmt);

		// add all pictures
		for (Picture pic : album.getPictures())
		{
			addPictureToAlbumById(pic, albumId);
		}
	}
}

void DatabaseAccess::deleteAlbum(const std::string& albumName, int userId)
{
	std::string deleteAlbumsQuery = "DELETE FROM albums WHERE user_id = " + std::to_string(userId) + " AND name = '" + albumName + "';";

	// delete all albums 
	if (sqlite3_exec(this->_DB, deleteAlbumsQuery.c_str(), nullptr, nullptr, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		std::cerr << "Error: couldn't DELETE albums" << std::endl;
	}
	else // delete all depending data
	{
		this->finalizeAlbumRemoval();
	}
}

bool DatabaseAccess::doesAlbumExists(const std::string& albumName, int userId)
{
	std::string countQuery = "SELECT COUNT(*) FROM albums WHERE name = '" + albumName + "' AND user_id = " + std::to_string(userId) + "; ";

	sqlite3_stmt* stmt;
	if (sqlite3_prepare_v2(this->_DB, countQuery.c_str(), -1, &stmt, NULL))
	{
		std::cerr << "Error At doesAlbumExist: couldn't find any existing album" << std::endl;
		return false;
	}
	else // check if count isn't 0
	{
		// send statement and receive count 
		sqlite3_step(stmt);
		int count = sqlite3_column_int(stmt, 0);
		sqlite3_finalize(stmt);
		return count > 0;
	}
}

Album DatabaseAccess::openAlbum(const std::string& albumName)
{
	std::vector < std::map<std::string, std::string> > albumRecords;
	std::string sqlQuery("SELECT * FROM albums WHERE name = '" + albumName + "';");

	// send the query to the database (save the answer inside albumRecords)
	if (sqlite3_exec(this->_DB, sqlQuery.c_str(), DatabaseHelper::generalObjectCallback, &albumRecords, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		std::cerr << "Error: couldn't SELECT albums" << std::endl;
	}
	else
	{
		if (albumRecords.empty()) // if empty no album with that name
		{
			throw MyException("No album with name " + albumName + " exists");
		}
		else
		{
			for (std::map<std::string, std::string> record : albumRecords)
			{
				return this->data_record_to_album(record); // return the first instance of the album	
			}
		}
	}

	throw MyException("No album with name " + albumName + " exists");
}

/*======================================================picture related======================================================*/
void DatabaseAccess::addPictureToAlbumById(const Picture& pic, int album_id)
{
	std::string picQuery = "INSERT INTO pictures (id, name, location, creation_date, album_id) VALUES(?, ?, ?, ?, ?);";
	sqlite3_stmt* stmt;

	// prepare statement
	if (sqlite3_prepare_v2(this->_DB, picQuery.c_str(), -1, &stmt, NULL)) // check that nothing went wrong
	{
		std::cerr << "Error: couldn't prepare the INSERT picture statement" << std::endl;
		return;
	}
	else // add picture and it's tags to the database
	{
		// bind values to the statement
		sqlite3_bind_int(stmt, 1, pic.getId()); // bind the id
		sqlite3_bind_text(stmt, 2, pic.getName().c_str(), -1, NULL); // bind the name
		sqlite3_bind_text(stmt, 3, pic.getPath().c_str(), -1, NULL); // bind the location
		sqlite3_bind_text(stmt, 4, pic.getCreationDate().c_str(), -1, NULL); // bind the creation date
		sqlite3_bind_int(stmt, 5, album_id); // bind the album id

		// execute the statement
		sqlite3_step(stmt);
		sqlite3_finalize(stmt);

		for (int userID : pic.getUserTags()) // insert all tags
		{
			std::string sqlQuery = "INSERT INTO tags (picture_id, user_id) VALUES(" + std::to_string(pic.getId()) + ", " + std::to_string(userID) + ");";
			sqlite3_exec(this->_DB, sqlQuery.c_str(), nullptr, nullptr, NULL);
		}
	}
}

void DatabaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
	std::string findIdQuery = "SELECT id FROM albums WHERE name = '" + albumName + "';";

	// get all album_ids
	sqlite3_stmt* stmt;
	if (sqlite3_prepare_v2(this->_DB, findIdQuery.c_str(), -1, &stmt, NULL))
	{
		std::cerr << "Error: At addPictureToAlbumByName";
		throw ItemNotFoundException("Album not exists: ", albumName);
	}
	else // add picture
	{
		bool albumExists = false;

		while (sqlite3_step(stmt) != SQLITE_DONE) // add the picture to all the albums with the requested name
		{
			albumExists = true;
			this->addPictureToAlbumById(picture, sqlite3_column_int(stmt, 0));
		}
		sqlite3_finalize(stmt);

		if (!albumExists)
			throw ItemNotFoundException("Album not exists: ", albumName);
	}
}

void DatabaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
	std::string subQuery = "(SELECT id FROM albums WHERE name = '" + albumName + "');";
	std::string deletePicturesQuery = "DELETE FROM pictures WHERE name = '" + pictureName + "' AND album_id = " + subQuery;

	// delete all pictures
	if (sqlite3_exec(this->_DB, deletePicturesQuery.c_str(), nullptr, nullptr, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		std::cerr << "Error At removePictureFromAlbumByName: couldn't DELETE pictures" << std::endl;
	}
	else // delete all depending data
	{
		this->finalizePictureRemoval();
	}
}

void DatabaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	std::string subQuery = "(SELECT id FROM albums WHERE name = '" + albumName + "');";
	std::string findIdQuery = "SELECT id FROM pictures WHERE name = '" + pictureName + "' AND album_id = " + subQuery;

	// get all picture ids
	sqlite3_stmt* stmt;
	if (sqlite3_prepare_v2(this->_DB, findIdQuery.c_str(), -1, &stmt, NULL))
	{
		std::cerr << "Error At tagUserInPicture: couldn't find picture id" << std::endl;
		throw ItemNotFoundException("Picture not exists: ", albumName);
	}
	else // add tag
	{
		bool pictureExists = false;

		while (sqlite3_step(stmt) != SQLITE_DONE) // add the tag to all the picture ids 
		{
			pictureExists = true;

			std::string addTagQuery = "INSERT INTO tags (user_id, picture_id) VALUES(" + std::to_string(userId) + ", " + std::to_string(sqlite3_column_int(stmt, 0)) + ");";
			sqlite3_exec(this->_DB, addTagQuery.c_str(), nullptr, nullptr, NULL);
		}
		sqlite3_finalize(stmt);

		if (!pictureExists)
			throw ItemNotFoundException("Photo not exists: ", albumName);
	}
}

void DatabaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	std::string subSubQuery = "(SELECT id FROM albums WHERE name = '" + albumName + "'));";
	std::string subQuery = "(SELECT id FROM pictures WHERE name = '" + pictureName + "' AND album_id = " + subSubQuery;
	std::string deleteTagsQuery = "DELETE FROM tags WHERE user_id = " + std::to_string(userId) + " AND picture_id = " + subQuery;

	// delete all matching tags 
	if (sqlite3_exec(this->_DB, deleteTagsQuery.c_str(), nullptr, nullptr, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		std::cerr << "Error At untagUserInPicture: couldn't DELETE tags" << std::endl;
	}
}

/*======================================================users related======================================================*/
void DatabaseAccess::printUsers()
{
	std::vector<User> allUsers;

	if (sqlite3_exec(this->_DB, "SELECT * FROM users", DatabaseHelper::userTableCallback, &allUsers, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		std::cerr << "Error At printUsers: couldn't SELECT users" << std::endl;
	}
	else
	{
		std::cout << "Users list:" << std::endl;
		std::cout << "-----------" << std::endl;
		for (const auto& user : allUsers) {
			std::cout << user << std::endl;
		}
	}
}

User DatabaseAccess::getUser(int userId)
{
	std::vector<User> allUsers;
	std::string getUserQuery = "SELECT * FROM users WHERE id = " + std::to_string(userId) + "; ";

	// get the user with the user id
	if (sqlite3_exec(this->_DB, getUserQuery.c_str(), DatabaseHelper::userTableCallback, &allUsers, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		std::cerr << "Error At getUser: couldn't SELECT user" << std::endl;
		throw ItemNotFoundException("User", userId);
	}
	else
	{
		if (allUsers.empty()) // if empty no user with that id
		{
			throw ItemNotFoundException("User", userId);;
		}
		else // if not empty return the user
		{
			for (User user : allUsers)
				return user;
		}
	}
}

void DatabaseAccess::createUser(User& user)
{
	std::string insertUserQuery = "INSERT INTO users (id, name) VALUES(" + std::to_string(user.getId()) + ", '" + user.getName() + "');";

	// insert user into the data base
	if (sqlite3_exec(this->_DB, insertUserQuery.c_str(), nullptr, nullptr, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		std::cerr << "Error At createUser: couldn't INSERT user" << std::endl;
	}
}

void DatabaseAccess::deleteUser(const User& user)
{
	std::string deleteUserQuery = "DELETE FROM users WHERE id = " + std::to_string(user.getId()) + "; ";

	// delete user from the database
	if (sqlite3_exec(this->_DB, deleteUserQuery.c_str(), nullptr, nullptr, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		std::cerr << "Error At deleteUser: couldn't DELETE user" << std::endl;
	}
	else
	{
		this->finalizeUserRemoval();
	}
}

bool DatabaseAccess::doesUserExists(int userId)
{
	std::string countQuery = "SELECT COUNT(*) FROM users WHERE id = " + std::to_string(userId) + "; ";

	sqlite3_stmt* stmt;
	if (sqlite3_prepare_v2(this->_DB, countQuery.c_str(), -1, &stmt, NULL))
	{
		std::cerr << "Error At doesUserExists: couldn't count user" << std::endl;
		return false;
	}
	else // check if count isn't 0
	{
		// send statement and receive count 
		sqlite3_step(stmt);
		int count = sqlite3_column_int(stmt, 0);
		sqlite3_finalize(stmt);
		return count > 0;
	}
}

/*======================================================user statistics======================================================*/
int DatabaseAccess::countAlbumsOwnedOfUser(const User& user)
{
	std::string countQuery = "SELECT COUNT(*) FROM albums WHERE user_id = " + std::to_string(user.getId()) + "; ";

	sqlite3_stmt* stmt;
	if (sqlite3_prepare_v2(this->_DB, countQuery.c_str(), -1, &stmt, NULL))
	{
		std::cerr << "Error At countAlbumsOwnedOfUser: couldn't count albums of user" << std::endl;
		return 0;
	}
	else // check count
	{
		// send statement and receive count 
		sqlite3_step(stmt);
		int albumsCount = sqlite3_column_int(stmt, 0);
		sqlite3_finalize(stmt);
		return albumsCount;
	}
}

int DatabaseAccess::countAlbumsTaggedOfUser(const User& user)
{
	std::string subQuery = "(SELECT DISTINCT picture_id FROM tags where user_id = " + std::to_string(user.getId()) + ");";
	std::string countQuery = "SELECT DISTINCT COUNT(album_id) FROM pictures WHERE id = " + subQuery;

	sqlite3_stmt* stmt;
	if (sqlite3_prepare_v2(this->_DB, countQuery.c_str(), -1, &stmt, NULL))
	{
		std::cerr << "Error At countAlbumsTaggedOfUser: couldn't count albums tagged" << std::endl;
		return 0;
	}
	else // check count
	{
		// send statement and receive count 
		sqlite3_step(stmt);
		int tagsCount = sqlite3_column_int(stmt, 0);
		sqlite3_finalize(stmt);
		return tagsCount;
	}
}

int DatabaseAccess::countTagsOfUser(const User& user)
{
	std::string countQuery = "SELECT COUNT(*) FROM tags WHERE user_id = " + std::to_string(user.getId()) + "; ";

	sqlite3_stmt* stmt;
	if (sqlite3_prepare_v2(this->_DB, countQuery.c_str(), -1, &stmt, NULL))
	{
		std::cerr << "Error At countTagsOfUser: couldn't count tags" << std::endl;
		return 0;
	}
	else // check count
	{
		// send statement and receive count 
		sqlite3_step(stmt);
		int tagsCount = sqlite3_column_int(stmt, 0);
		sqlite3_finalize(stmt);
		return tagsCount;
	}
}

float DatabaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
	int albumsTaggedCount = countAlbumsTaggedOfUser(user);

	if (0 == albumsTaggedCount) {
		return 0;
	}

	return static_cast<float>(countTagsOfUser(user)) / albumsTaggedCount;
}

/*===========================================================queries===========================================================*/
User DatabaseAccess::getTopTaggedUser()
{
	std::vector<User> allUsers;
	std::string subQuery = "(SELECT user_id FROM tags GROUP BY user_id ORDER BY COUNT(*) DESC LIMIT 1);";
	std::string getMostTaggedUserQuery = "SELECT * FROM users WHERE id = " + subQuery;

	// get the most tagged user
	if (sqlite3_exec(this->_DB, getMostTaggedUserQuery.c_str(), DatabaseHelper::userTableCallback, &allUsers, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		std::cerr << "Error At getTopTaggedUser: couldn't SELECT user" << std::endl;
		throw MyException("There isn't any tagged users.");
	}
	else
	{
		if (allUsers.empty()) // if empty no user with that id
		{
			throw MyException("There isn't any tagged users.");
		}
		else // if not empty return the user
		{
			for (User user : allUsers)
				return user;
		}
	}
}

Picture DatabaseAccess::getTopTaggedPicture()
{
	std::vector< std::map<std::string, std::string> > allPicsRecord;
	std::string subQuery = "(SELECT picture_id FROM tags GROUP BY picture_id ORDER BY COUNT(*) DESC LIMIT 1);";
	std::string getMostTaggedPicQuery = "SELECT * FROM pictures WHERE id = " + subQuery;

	// get the most tagged pic
	if (sqlite3_exec(this->_DB, getMostTaggedPicQuery.c_str(), DatabaseHelper::generalObjectCallback, &allPicsRecord, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		std::cerr << "Error At getTopTaggedPicture: couldn't SELECT pic" << std::endl;
		throw MyException("There isn't any tagged picture.");
	}
	else
	{
		if (allPicsRecord.empty()) // if empty no picture with that id
		{
			throw MyException("There isn't any tagged picture.");
		}
		else // if not empty return the pic
		{
			return this->data_record_to_picture(allPicsRecord[0]);
		}
	}
}

std::list<Picture> DatabaseAccess::getTaggedPicturesOfUser(const User& user)
{
	std::vector< std::map<std::string, std::string> > allPicsRecord;
	std::list<Picture> allPics;

	std::string subQuery = "(SELECT DISTINCT picture_id FROM tags where user_id = " + std::to_string(user.getId()) + ");";
	std::string getTaggedPicsQuery = "SELECT * FROM pictures WHERE id = " + subQuery;

	// get the pictures the user is tagged in
	if (sqlite3_exec(this->_DB, getTaggedPicsQuery.c_str(), DatabaseHelper::generalObjectCallback, &allPicsRecord, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		std::cerr << "Error At getTaggedPicturesOfUser: couldn't SELECT pics" << std::endl;
	}
	else
	{
		if (allPicsRecord.empty()) // if empty no picture with that id
		{
			throw MyException("There isn't any tagged picture.");
		}
		else // if not empty return the pic
		{
			for (std::map<std::string, std::string> picRecord : allPicsRecord)
				allPics.push_back(this->data_record_to_picture(picRecord));
		}
	}
	return allPics;
}