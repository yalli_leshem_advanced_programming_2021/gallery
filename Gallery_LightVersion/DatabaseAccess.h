#pragma once
#include <list>
#include <map>
#include <vector>
#include <sqlite3.h>
#include <iostream>
#include "Album.h"
#include "User.h"
#include "IDataAccess.h"
#include "DatabaseHelper.h"

class DatabaseAccess : public IDataAccess
{
public:
	DatabaseAccess() = default;
	virtual ~DatabaseAccess();

	// album related
	const std::list<Album> getAlbums() override;
	const std::list<Album> getAlbumsOfUser(const User& user) override;
	void createAlbum(const Album& album) override;
	void deleteAlbum(const std::string& albumName, int userId) override;
	bool doesAlbumExists(const std::string& albumName, int userId) override;
	Album openAlbum(const std::string& albumName) override;
	void closeAlbum(Album& pAlbum) override {};
	void printAlbums() override;

	// picture related
	/// <summary>
	/// function inserts a picture to the database
	/// </summary>
	/// <param name="pic">the pic</param>
	/// <param name="album_id">the album id</param>
	void addPictureToAlbumById(const Picture& pic, int album_id);

	void addPictureToAlbumByName(const std::string& albumName, const Picture& picture) override;
	void removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName) override;
	void tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) override;
	void untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) override;

	// user related
	void printUsers() override;
	void createUser(User& user) override;
	void deleteUser(const User& user) override;
	bool doesUserExists(int userId) override;
	User getUser(int userId) override;

	// user statistics
	int countAlbumsOwnedOfUser(const User& user) override;
	int countAlbumsTaggedOfUser(const User& user) override;
	int countTagsOfUser(const User& user) override;
	float averageTagsPerAlbumOfUser(const User& user) override;

	// queries
	User getTopTaggedUser() override;
	Picture getTopTaggedPicture() override;
	std::list<Picture> getTaggedPicturesOfUser(const User& user) override;

	// database related 
	bool open() override;
	void close() override;
	void clear() override {};

private:
	sqlite3* _DB;

	/// <summary>
	/// convert the data we got from the database into a new album object
	/// </summary>
	/// <param name="data_record">the data we got from the database - the key is the column name and the value is the value</param>
	/// <returns>a album object</returns>
	Album data_record_to_album(std::map<std::string, std::string> data_record);

	/// <summary>
	/// convert the data we got from the database into a new picture object
	/// </summary>
	/// <param name="data_record">the data we got from the database - the key is the column name and the value is the value</param>
	/// <returns>a picture object</returns>
	Picture data_record_to_picture(std::map<std::string, std::string> data_record);

	//---------------------------delete functions------------------------------//

	/// <summary>
	/// after removing an album from the database use this function to remove all data thats related to and depending on the album
	/// </summary>
	void finalizeAlbumRemoval();

	/// <summary>
	/// after removing a picture from the database use this function to remove all data thats related to and depending on the picture
	/// </summary>
	void finalizePictureRemoval();

	/// <summary>
	/// after removing a user from the database use this function to remove all data thats related to and depending on the user
	/// </summary>
	void finalizeUserRemoval();
};

