﻿#pragma once
#include <list>
#include "Album.h"
#include "User.h"
#include "IDataAccess.h"

class MemoryAccess : public IDataAccess
{

public:
	MemoryAccess() = default;
	virtual ~MemoryAccess() = default;

	// album related
	const std::list<Album> getAlbums() override;
	const std::list<Album> getAlbumsOfUser(const User& user) override;
	void createAlbum(const Album& album) override;
	void deleteAlbum(const std::string& albumName, int userId) override;
	bool doesAlbumExists(const std::string& albumName, int userId) override;
	Album openAlbum(const std::string& albumName) override;
	void closeAlbum(Album &pAlbum) override;
	void printAlbums() override;

	// picture related
	void addPictureToAlbumByName(const std::string& albumName, const Picture& picture) override;
	void removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName) override;
	void tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) override;
	void untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) override;

	// user related
	void printUsers() override;
	void createUser(User& user) override;
	void deleteUser(const User& user) override;
	bool doesUserExists(int userId) override;
	User getUser(int userId) override;

	// user statistics
	int countAlbumsOwnedOfUser(const User& user) override;
    int countAlbumsTaggedOfUser(const User& user) override;
	int countTagsOfUser(const User& user) override;
	float averageTagsPerAlbumOfUser(const User& user) override;

	// queries
	User getTopTaggedUser() override;
	Picture getTopTaggedPicture() override;
	std::list<Picture> getTaggedPicturesOfUser(const User& user) override;

	bool open() override;
	void close() override {};
	void clear() override;

private:
	std::list<Album> m_albums;
	std::list<User> m_users;

	auto getAlbumIfExists(const std::string& albumName);

	Album createDummyAlbum(const User& user);

	// delete user functions
	void cleanUserData(const User& userId);

	/// <summary>
	/// function removes all albums owned by the user
	/// </summary>
	/// <param name="userId">the user that will have all his albums deleted</param>
	void removeUserAlbums(const User& userId);

	/// <summary>
	/// function untags the user from all the pictures he is tagged in
	/// </summary>
	/// <param name="userId">the user</param>
	void untagUserInAllPictures(const User& userId);
};
