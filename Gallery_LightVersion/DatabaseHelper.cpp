#include "DatabaseHelper.h"


int DatabaseHelper::generalObjectCallback(void* data, int argc, char** argv, char** azColName)
{
	std::vector< std::map<std::string, std::string> >* rows = (std::vector< std::map<std::string, std::string> >*)data;
	std::map<std::string, std::string> col;

	for (int i = 0; i < argc; i++) {
		std::pair<std::string, std::string> keyVal;
		keyVal.first = azColName[i]; // put the column name in the first field
		keyVal.second = argv[i]; // put the column value in the second one
		col.insert(keyVal);
	}

	if (!col.empty())
		rows->push_back(col);
	return 0;
}

int DatabaseHelper::userTableCallback(void* data, int argc, char** argv, char** azColName)
{
	int id = 0;
	std::string name;
	std::vector<User>* allUsers = (std::vector<User>*)data;

	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "ID") // cast the id into int
		{
			id = atoi(argv[i]);
		}

		if (std::string(azColName[i]) == "NAME") // cast the name into string
		{
			name = argv[i];
		}
	}

	if (!(id == 0 || name.empty())) // check that the data was not empty
		allUsers->push_back(User(id, name));

	return 0;
}
